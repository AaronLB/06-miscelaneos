import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `
    <p [style.fontSize.px]="size">
      Hi World..... this a new tag
    </p>

    <button class="btn btn-primary" (click)="size = size + 5">
    <i class="fa fa-plus-circle" aria-hidden="true"></i>
    </button>

    <button class="btn btn-danger" [style.marginLeft.px]="'5'" (click)="size = size - 5">
    <i class="fa fa-minus-circle" aria-hidden="true"></i>
    </button>
  `,
  styles: [
  ]
})
export class NgStyleComponent {

  size:number=10;
}
