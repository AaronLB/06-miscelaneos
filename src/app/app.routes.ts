import { NgModule } from "@angular/core";

import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./components/home/home.component";
import { UserComponent } from "./components/user/user.component";

import { USER_ROUTES } from "./components/user/user.routes";


const APP_ROUTES: Routes = [
    { path: 'home', component:HomeComponent},
    {
        path: 'user/:10', 
        component:UserComponent,
        children: USER_ROUTES
        // [ // rutas hijas
        //     { path:'new', component:UserNewComponent },
        //     { path:'edit', component:UserEditComponent },
        //     { path:'detail', component:UserDetailComponent },
        //     { path: '**', pathMatch:'full', redirectTo:'new' } // ruta por defecto
        // ]
    },
    { path: '**', pathMatch:'full', redirectTo:'home' }
]

@NgModule({
    imports:[RouterModule.forRoot(APP_ROUTES)],
    exports:[RouterModule]
})

export class AppRoutingModule{} 