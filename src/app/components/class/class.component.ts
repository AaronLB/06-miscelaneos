import { Component } from '@angular/core';

@Component({
  selector: 'app-class',
  templateUrl:'./class.component.html',
  styles: [
  ]
})
export class ClassComponent {

  alert:string = "alert-danger";

  louding:boolean=false;

  porperty:any = {
    danger:false
  }
  constructor(){
    
  }

  execute(){
    this.louding = true;
    setTimeout(() => this.louding = false, 3000);
  }
}
