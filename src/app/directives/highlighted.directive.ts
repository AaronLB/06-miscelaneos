import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlighted]'
})
export class HighlightedDirective {

  constructor(
    private el: ElementRef
  ) { 
    // console.log('Directives Loading');
    // el.nativeElement.style.backgroundColor = "yellow";
  }

  @Input("appHighlighted") newColor?:string;

  @HostListener('mouseenter') mouseEntro(){
    console.log(this.newColor);
    this.highlighted( this.newColor || "yellow" );
  }
  @HostListener('mouseleave') mouseExit(){
    this.highlighted( '' );
  }

  private highlighted ( color:string){
    this.el.nativeElement.style.backgroundColor = color;
  }
}
