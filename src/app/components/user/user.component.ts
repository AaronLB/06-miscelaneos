import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {

  constructor(
    private router:ActivatedRoute
  ) {
    this.router.params.subscribe( items => {
      console.log('father router');
      
      console.log(items);
      
    } )
  }
}
